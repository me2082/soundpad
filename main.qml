import QtQuick
import QtQuick.Window
import QtMultimedia
import QtQuick.Layouts

Window {
    width: 640
    height: 480
    visible: true
    title: qsTr("Hello World")

    Rectangle {
        id: one
        x: 0
        y: 0
        width: 100
        height: 100
        color: "aliceblue"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: onetext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }


        MediaPlayer {
            id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
            id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "aliceblue"
        }
    }

    Rectangle {
        id: two
        x: 105
        y: 0
        width: 100
        height: 100
        color: "antiquewhite"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: twotext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "antiquewhite"
        }
    }

    Rectangle {
        id: three
        x: 210
        y: 0
        width: 100
        height: 100
        color: "aqua"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: threetext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "aqua"
        }
    }

    Rectangle {
        id: four
        x: 315
        y: 0
        width: 100
        height: 100
        color: "aquamarine"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: fourtext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "aquamarine"
        }
    }

    Rectangle {
        id: five
        x: 0
        y: 105
        width: 100
        height: 100
        color: "azure"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: fivetext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "azure"
        }
    }

    Rectangle {
        id: six
        x: 105
        y: 105
        width: 100
        height: 100
        color: "beige"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: sixtext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: playMusic.source
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "beige"
        }
    }

    Rectangle {
        id: seven
        x: 210
        y: 105
        width: 100
        height: 100
        color: "bisque"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: seventext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "bisque"
        }
    }

    Rectangle {
        id: eight
        x: 315
        y: 105
        width: 100
        height: 100
        color: "blanchedalmond"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: eighttext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "blanchedalmond"
        }
    }

    Rectangle {
        id: nine
        x: 0
        y: 210
        width: 100
        height: 100
        color: "blue"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: ninetext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "blue"
        }
    }

    Rectangle {
        id: ten
        x: 105
        y: 210
        width: 100
        height: 100
        color: "blueviolet"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: tentext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "blueviolet"
        }
    }

    Rectangle {
        id: eleven
        x: 210
        y: 210
        width: 100
        height: 100
        color: "brown"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: eleventext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "brown"
        }
    }

    Rectangle {
        id: twelve
        x: 315
        y: 210
        width: 100
        height: 100
        color: "burlywood"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: twelvetext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "burlywood"
        }
    }

    Rectangle {
        id: thirteen
        x: 0
        y: 315
        width: 100
        height: 100
        color: "cadetblue"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: thirteentext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "cadetblue"
        }
    }

    Rectangle {
        id: fourteen
        x: 105
        y: 315
        width: 100
        height: 100
        color: "chartreuse"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: fourteentext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "chartreuse"
        }
    }

    Rectangle {
        id: fifthteen
        x: 210
        y: 315
        width: 100
        height: 100
        color: "chocolate"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: fifthteentext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "chocolate"
        }
    }

    Rectangle {
        id: sixteen
        x: 315
        y: 315
        width: 100
        height: 100
        color: "coral"
        border.color: "black"
        border.width: 5
        radius: 10

        Text {
            id: sixteentext
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            text: qsTr("text")
        }

        MediaPlayer {
           // id: playMusic
            source: "qrc:/new/prefix1/Sounds/crabmp3.mp3"
            audioOutput: AudioOutput {}
        }
        MouseArea {
           // id: playArea
            anchors.fill: parent
            onPressed: {playMusic.play(); this.parent.color = "black";}
            onReleased: this.parent.color = "coral"

        }
    }
}

